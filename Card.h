#ifndef CARD_H
#define CARD_H

#include "Errors.h"
#include "DeckandHand.h"
#include <string>
#include<vector>
#include<stdbool.h>

//A struct for a card is declared.
//A card has a suit and rank and a method for converting a char to its suit and rank.
//Cards also have a operator< so that each card can be compared and sorted, first on rank, then on suit.

struct Card {
	Card();
	enum struct suit : char {
		clubs = 'c',
		diamonds = 'd',
		hearts = 'e',
		spades = 'f'
	};
	//Each rank has an int value that are continuous from 2-ace
	//This gives us the relative rank of each card.
	enum struct rank : char {
		two = 'a', three = 'b', four = 'c', five = 'd', six = 'e', seven = 'f', eight = 'g',
		nine = 'h', ten = 'i',
		jack = 'j', queen = 'k', king = 'l', ace = 'm'
	};

	Card(suit s, rank r);
	suit s_;
	rank r_;
	Card::suit charToSuit(char & c);
	Card::rank charToRank(char & b);
	std::string getCardSuit(Card & c);
	std::string getCardRank(Card & c);
	bool operator < (const Card & c1) const{
		if (r_ != c1.r_)
		{
			return (r_ < c1.r_);
		}

		return s_<c1.s_;
};
};


int parse(std::vector<Card> & v, char* s);
std::string printV(const std::vector<Card> & v);
bool checkString3(std::string str);
bool checkString2(std::string str);
int err(std::string, std::string);

#endif
