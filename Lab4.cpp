
// Lab2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Game.h"
#include <iostream>
using namespace std;

int usage(string program, string args, errorCode err) {
	cout << "Usage: " << program << " " << args << endl;
	return err; // usage error code
}

int main(int argc, char* argv[]){
	// number of expected arguments:
	const int num_args = 1;

	// check for correct usage: 
	char * program = argv[0];
	if (argc < 1) {
		return usage(program, " does not take any arguments!", errorCode::USAGE); // too few arguments
	}

	play();
	return 0;
}

bool playAgain() {
	while (true) {
		// ask if the user would like to start a new game:
		string play;
		cout << "Would you like to play a new game? (Y/N): ";
		cin >> play;

		// start a new game:
		if (play == "Y" || play == "y") {
			return true;
		}
		// quit the program:
		else if (play == "N" || play == "n") {
			return false;
		}
		// invalid response, prompt again:
		else {
			cout << "Invalid response! Please enter Y/N.";
			continue;
		}
	}
}

void checkPlayerChips();
int play() {

	// start a game:
	cout << "Enter the game you want to play: ";
	string gameName;
	cin >> gameName;
	try {
		Game::start_game(gameName);
	}
	catch (errorCode &e) {
		cout << errorString(e) << endl;
		bool again = playAgain();
		if (again) {
			play();
		}
		else {
			return (int)e;
		}
	}

	// get a pointer to the game:
	shared_ptr<Game> g;
	try {
		g = Game::instance();
	}
	catch (errorCode &e) {
		cout << errorString(e) << endl;
		(*g).stop_game();
		bool again = playAgain();
		if (again) {
			play();
		}
		else {
			return (int)e;
		}
	}

	// add players to the game:
	unsigned int num_players = 0;
	while (true){
		string playerName;
		// check if there are too many players:
		unsigned int max_players = (*g).deckMain.size() / (*g).hand_size;
		if (num_players > max_players){
			cout << errorString(NOT_ENOUGH_CARDS) << endl;
			(*g).stop_game();
			bool again = playAgain();
			if (again) {
				play();
			}
			else {
				return (int)errorCode::NOT_ENOUGH_CARDS;
			}
		}
		else {
			// continually prompt for players to add until the user enters "No"
			cout << "Please type the name of players one at a time that you would like to add to the game, or 'No' to continue without adding any more players:  ";
			cin >> playerName;
			if (playerName == "no" || playerName == "No" || playerName == "NO"){
				break;
			}
			else {
				++num_players;
				try{
					(*g).add_player(playerName);
				}
				catch (errorCode &e){
					cout << errorString(e) << endl;
					(*g).stop_game();
					bool again = playAgain();
					if (again) {
						play();
					}
					else {
						return (int)e;
					}
				}
			}
		}
	}
	(*g).checkPlayerChips();
	// check if there are too few players:
	if (num_players < 2){
		cout << errorString(NOT_ENOUGH_PLAYERS) << endl;
		(*g).stop_game();
		bool again = playAgain();
		if (again) {
			play();
		}
		else {
			return (int)errorCode::NOT_ENOUGH_PLAYERS;
		}
	}
	else {
		// continue going through before_round, round, and after_round while you have at least two players:
		while ((*g).numPlayers > 1){
			try{
				(*g).before_round();
			}
			catch (errorCode e){
				cout << errorString(e) << endl;
			}
			try{
				(*g).round();
			}
			catch (errorCode e){
				cout << errorString(e) << endl;
				(*g).stop_game();
				bool again = playAgain();
				if (again) {
					play();
				}
				else {
					return (int)e;
				}
			}
			try{
				(*g).after_round();
			}
			catch (errorCode e){
				cout << errorString(e) << endl;
				(*g).stop_game();
				bool again = playAgain();
				if (again) {
					play();
				}
				else {
					return (int)e;
				}
			}
			cin.ignore(); //ignore the first character in a round because it is /n
			// if you remove too many players, stop the game:
			if ((*g).numPlayers < 2){
				cout << errorString(NOT_ENOUGH_PLAYERS) << endl;
				bool again = playAgain();
				if (again) {
					play();
				}
				else {
					return (int)errorCode::NOT_ENOUGH_PLAYERS;
				}
			}
			else {
				continue;
			}
		}
	}

	// end the game:
	(*g).stop_game();
	bool again = playAgain();
	if (again) {
		play();
	}
	else {
		return 0;
	}
	return 0;
}


