#include "stdafx.h"
#include <vector>
#include "Card.h"
#include "pokerranks.h"
#include<stdbool.h>
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
using namespace std;

//Check hand for flush.
//We push back the char value of each suit of each card into a vector then sort it
//Once sorted, if the char value of the first card equals the char value of the last
//All suits must be the same and we have a flush
bool isFlush(std::vector<Card> v){
	vector<char> fl;
	for (int i = 0; i < 5; ++i){
		fl.push_back(tolower((char)v[i].s_));
	}
	sort(fl.begin(), fl.end());
	return (fl[0] == fl[4]);
}

//Function to check if our hand is a straight.
// return true if it is
bool isStraight(vector<Card> &v){
	vector<int> checkStraight;
	//iterate through the hand and place the int value associated with each card in a vector
	//Int values are defined in the Card struct but go in ascending order from 2-ace
	for (int i = 0; i < 5; i++){
		Card car = v[i];
		int ra = (int)car.r_;
		checkStraight.push_back(ra);
	}

	//Compare each card+1 to the next card.
	//If each card+1 equals the next card, then all cards are in a continuous order
	// and we have a straight.
	if (checkStraight[0] + 1 == checkStraight[1]
		&& checkStraight[1] + 1 == checkStraight[2]
		&& checkStraight[2] + 1 == checkStraight[3]
		&& checkStraight[3] + 1 == checkStraight[4]){
		return true;
	}
	return false;
}

//Check hand for full house
bool isFullHouse(const std::vector<Card> &hand){
	//with a sorted hand there are only 2 configurations for a full house
	// XXXYY or XXYYY
	if (hand[0].r_ == hand[1].r_ &&
		(hand[3].r_ == hand[2].r_ && hand[4].r_ == hand[2].r_)){
		return true;
	}
	else if (hand[3].r_ == hand[4].r_ &&
		(hand[0].r_ == hand[2].r_ && hand[1].r_ == hand[2].r_)){
		return true;
	}
	return false;
}

//Check for three of a kind.
bool isThreeK(const std::vector<Card> &hand){
	//There are only 2 configurations for 3 of a kind.
	return((hand[1].r_ == hand[2].r_ && hand[0].r_ == hand[2].r_)
		|| (hand[2].r_ == hand[3].r_ && hand[2].r_ == hand[4].r_)
		|| (hand[1].r_ == hand[2].r_ && hand[2].r_ == hand[3].r_));
}

/**
* Check the number of pairs in a hand
* numPairs can equal 0,1,2
* Take in a hand and return number of pairs.
*/
int numPairs(const std::vector<Card> &hand, int & p){
	if (hand[0].r_ == hand[1].r_){
		++p;
		if (hand[2].r_ == hand[3].r_ || hand[3].r_ == hand[4].r_){
			++p;
		}
	}
	else if (hand[1].r_ == hand[2].r_){
		++p;
		if (hand[3].r_ == hand[4].r_){
			++p;
		}
	}
	else if (hand[2].r_ == hand[3].r_){
		++p;
		if (hand[0].r_ == hand[1].r_){
			++p;
		}
	}
	else if (hand[3].r_ == hand[4].r_){
		++p;
		if (hand[1].r_ == hand[2].r_ || hand[0].r_ == hand[1].r_){
			++p;
		}
	}
	return p;
}
/**
* Our main method for checking poker hands.
* We take in a const reference to a vector
* hand is a temporary vector that holds 5 cards at a time from the main vector
* While there is at least 5 cards still left in our vector we check each hand for its poker rank
* Hands are checked for each rank starting with the highest ranking hand to no rank.
* This ensures that we print the highest rank of a hand. i.e. A full house is also 2 pairs but full house has a higher poker rank
* To keep track of the highest rank of our hand we use temporary variables that will fail for each lower ranked hand
* After checking all ranks we add 5 to our while loop variable so that we look at the next poker hand
* Return an int value associated with each rank.
*/
int pokerRank(const vector<Card> & v){
	
	if (v.size() < 5){
		return 0;
	}
	else{
		unsigned int i = 0;
		vector<Card> hand;
		auto first = v.begin();
		auto fifth = v.begin() + 5;

		while (i < v.size()){
			int pair = 0, flush = 0, straight = 0, fullHouse = 0, fourK = 0, threeK = 0;
			hand = { first + i, fifth + i };
			flush = isFlush(hand);
			int numPair_ = numPairs(hand, pair);
			if ((hand[0].r_ == hand[3].r_)){
				++fourK;
				return 8;
			}

			else if (hand[1].r_ == hand[4].r_){
				++fourK;
				return 8;
			}
			else if (flush == 1 && isStraight(hand)){

				++straight;
				return 9;

			}
			else if (flush == 0 && isFullHouse(hand)){
				++fullHouse;
				return 7;
			}

			else if (flush == 1 && straight == 0){
				return 6;
			}
			else if (flush == 0 && isStraight(hand)){
				++straight;

				return 5;
			}

			else if (flush == 0 && fourK == 0 && fullHouse == 0 && isThreeK(hand)){
				++threeK;
				return 4;
			}
			else if (numPair_ != 0){
				if (numPair_ == 2){
					return 3;
				}
				if (numPair_ == 1){
					return 2;
				}
			}
			else  {
				return 1;
			}
			i += 5;
		}
	}
	return 0;
}

