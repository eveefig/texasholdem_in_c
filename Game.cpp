#include "stdafx.h"
#include "Game.h"
#include "Errors.h"
#include "Card.h"
#include "DeckandHand.h"
#include "FiveCardDraw.h"
#include "SevenCardStud.h"
#include "TexasHoldEm.h"
#include<memory>
#include <algorithm>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

std::shared_ptr<Game> Game::gsp = nullptr;
string fiveCardDraw = "FIVECARDDRAW";
string sevenCardStud = "SEVENCARDSTUD";
string texasHoldEm = "TEXASHOLDEM";

//constructor initializes number of players to zero, the shared_ptr that points to the game to null, the vector
//of shared pointers that points to the players to empty, and the main deck to be empty
Game::Game()
: vsp(), deckMain(), numPlayers(0), hand_size(0) {}

//instance returns a copy of a shared_ptr to a game that has begun so the main method can access the game
std::shared_ptr<Game> Game::instance() {
	if (gsp == nullptr){ 
		throw errorCode::INSTANCE_NOT_AVAILABLE;
	}
	shared_ptr<Game> gspcopy = gsp;
	return gspcopy;
}

//start_game begins a game if the user inputs "FiveCardDraw" or "SevenCardDraw"
void Game::start_game(const std::string & s){
	string temp = s;
	transform(temp.begin(), temp.end(), temp.begin(), toupper);
	if (gsp != nullptr){ //if gsp is already pointing to something
		throw errorCode::GAME_ALREADY_STARTED;
	}
	else if (temp.find(fiveCardDraw) != string::npos){ //if the string does not include "FiveCardDraw"
		gsp = make_shared<FiveCardDraw>();
	}
	else if (temp.find(sevenCardStud) != string::npos){ //if the string does not include "FiveCardDraw"
		gsp = make_shared<SevenCardStud>();
	}
	else if (temp.find(texasHoldEm) != string::npos){ //if the string does not include "FiveCardDraw"
		gsp = make_shared<TexasHoldEm>();
	}
	else{ //if no game has started and "FiveCardDraw" is included in the string
		
	throw errorCode::UNKNOWN_GAME;
	}
}

//stop game ends the game by setting the game shared_ptr to nullptr
void Game::stop_game(){
	ofstream of;
	if (gsp == nullptr){
		throw errorCode::NO_GAME_IN_PROGRESS;
	} 
	else{
		for (auto p = (*gsp).vsp.begin(); p != (*gsp).vsp.end(); ++p) {
			string name = (**p).name;
			of.open(name);
			of << **p << endl;
			of.close();
		}
		gsp = nullptr;
	}
}

//add_player creates an additional player and pushes it into the vector of shared_ptrs of players
void Game::add_player(const string & s){
	const char * s2;
	s2 = s.c_str();
	char * c = (char *)s2;
	shared_ptr<Player> newPlayer = make_shared<Player>(c);

	//if we already have 10 players and we want to add more
	if (numPlayers == 10){
		throw errorCode::NOT_ENOUGH_CARDS;
	}
	//if the player already exists
	if (find_player((*newPlayer).name) != nullptr){
		throw errorCode::ALREADY_PLAYING;
	}

	else{
		vsp.push_back(newPlayer);
		++numPlayers;
	}
}

//find_player determines if a player of this name already exists in the game. No duplicate names are allowed
std::shared_ptr<Player> Game::find_player(const string & f) {
	for (size_t i = 0; i <vsp.size(); ++i){
		if ((*(vsp[i])).name == f){
			return vsp[i];
		}
	}
	return nullptr;
}

//leave asks if players want to leave
void Game::leave()  {
	string p;
	bool a = true;
	ofstream of;
	while (a){
		p = "";
		cout << "\nThe current players are: " << endl;
		for (unsigned int i = 0; i < numPlayers; ++i){
			cout << (*vsp[i]).name << endl;
		}
		cout << "\nPlease type the name of players one at a time that you would like to remove from the game, or 'No' to continue with all remaining players: ";
		cin >> p;
		if (p == "no" || p == "No" || p == "NO"){
			a = false;
		}
		else {
			if (find_player(p) == nullptr) {
				cout << p << " is not a player in the game and cannot be removed." << endl;
			}
			else {
				for (unsigned int i = 0; i < vsp.size(); ++i){
					if (p == (*vsp[i]).name){
						of.open(p);
						of << *vsp[i] << endl;
						of.close();

						auto it = vsp.begin() + i;
						rotate(vsp.begin(), it, vsp.end());
						reverse(vsp.begin(), vsp.end());
						vsp.pop_back();
						--numPlayers;

						if (numPlayers < 2){
							throw errorCode::NOT_ENOUGH_PLAYERS;
						}
						cout << p << " removed" << endl;
						break;
					}
				}
			}
		}
	}
}

//join helps the after_round method add players
void Game::join(){
	string q;
	bool b = true;
	while (b){
		q = "";
		cout << "\nPlease type the name of players one at a time that you would like to add to the game, or 'No' to continue without adding any more players:  ";
		cin >> q;
		//if user says "no", they do not want to add anyone
		if (q == "no" || q == "No" || q == "NO"){
			b = false;
		}
		else{
			shared_ptr<Player> pl = find_player(q);
			if (pl != nullptr) {
				if ((*pl).chips <= 0) {
					cout << (*pl).name <<", please reset your chip count to 20 (\"20\") or quit the game (\"q\")!" << endl;
					string input;
					cin >> input;
					if (input == "20") {
						(*pl).chips = 20;
						add_player(q);
						cout << q << " added" << endl;
					}
					else {
						cout << "YOU CAN'T SIT WITH US! (Not enough chips to play)!" << endl;
						// don't add the player
					}
				}
			}
			else {
				add_player(q);
				cout << q << " added" << endl;
			}
		}
	}
}

//destructor clears the vector of shared_ptrs of players and also clears the pointer to the game
Game::~Game(){
	vsp.clear();
	gsp = nullptr;
}

//Checks each player at beginning and end of a game to see if they have 0 chips.
// If they do, they must reset chips or be kicked from the game.
void Game::checkPlayerChips() {
	auto it = vsp.begin();
	for (unsigned int i = 0; i < vsp.size();) {
		if ((*vsp[i]).chips <= 0) {
			cout << (*vsp[i]).name<< ", please reset your chip count to 20 (\"20\") or quit the game (\"q\")!" << endl;
			string input;
			cin >> input;
			if (input == "20") {
				(*vsp[i]).chips = 20;
				++i;
			}
			else {
				cout << "YOU CAN'T SIT WITH US! (Not enough chips to play)!" << endl;
				// remove player
				auto it = vsp.begin() + i;
				rotate(vsp.begin(), it, vsp.end());
				reverse(vsp.begin(), vsp.end());
				vsp.pop_back();
				--numPlayers;
			}
		}
		else{
			++i;
		}
	}
}