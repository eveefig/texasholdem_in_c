#include "stdafx.h"
#include "SevenCardStud.h"
#include "Errors.h"
#include "Card.h"
#include "DeckandHand.h"
#include "pokerranks.h"
#include<memory>
#include <algorithm>
#include <string>
#include <istream>
#include <iostream>
#include <vector>
#include <ostream>
#include <fstream>
#include <sstream>

using namespace std;

class IntermediateGame;
class Game;
struct Card;
class Hand;
std::string getCardSuit(Card & c);
std::string getCardRank(Card & c);

void ante();
void dealdeck();
string getCardSuit(Card & c);
string getCardRank(Card & c);


SevenCardStud::SevenCardStud()
{
	hand_size = 7;
	numFolded = 0;
}

int SevenCardStud::before_round(){
	deckMain.shuffle();
	ante();
	return 0;
}


int SevenCardStud::round(){
	first_turn(); //first round deals 3 cards
	other_turns(); // all other rounds deal just one
	other_turns();
	other_turns();
	other_turns();
	return 0;
}

int SevenCardStud::after_round(){
	for (unsigned int i = 0; i < vsp.size(); ++i){
		vector<Card> temp = seven_choose_five((*vsp[i]).hand.getVec());
		sort(temp.begin(), temp.end());
		Hand h = Hand(temp);
		(*(vsp)[i]).hand = h;
	}
	sort(vsp.begin(), vsp.end(), poker_Rank_p);
	updateScores(vsp);
	moveCards(vsp);
	checkPlayerChips();

	leave();
	join();
	int newpos = (dealpos + 1) % vsp.size();
	dealpos = newpos;
	return 0;
}

int SevenCardStud::first_turn(){
	//deck has enough cards for everyone to get 7, deal 2 cards to each player
	int firstturncards = (numPlayers - numFolded) * 3;
	dealeachturn(firstturncards); //deal first round
	//Once all players have their cards we want to start a round of betting.

	int v = betting();
	while (v != 0){
		v = betting();
	}
	return 0;
}

int SevenCardStud::other_turns(){
	//deck has enough cards for everyone to get 7, deal 2 cards to each player
	int otherturncards = (numPlayers - numFolded);
	dealeachturn(otherturncards); //deal first round
	//Once all players have their cards we want to start a round of betting.

	int v = betting();
	while (v != 0){
		v = betting();
	}
	return 0;
}

void SevenCardStud::print(unsigned int yourpos){
	cout << "\n" << (*vsp[yourpos]).name << ", this is your current hand: " << (*vsp[yourpos]).hand << endl;
	//print everyone elses hand
	for (unsigned int i = 0; i < vsp.size() - 1; ++i){
		int printpos = (yourpos + i + 1) % (vsp.size());
		Player pl = *vsp[printpos];
		if (!pl.folded){
		if (pl.hand.size() >= 3){
				cout << pl.name << "'s hand: " << getCardRank(pl.hand[0]) << getCardSuit(pl.hand[0]) << " " << getCardRank(pl.hand[1]) << getCardSuit(pl.hand[1]) << " * ";
		}
		if (pl.hand.size() >= 4){
			cout << " * ";
		}
		if (pl.hand.size() >= 5){
			cout << " * ";
		}
		if (pl.hand.size() >= 6){
			cout << " * ";
		}
		if (pl.hand.size() == 7){
			cout << getCardRank(pl.hand[6]) << getCardSuit(pl.hand[6]);
		}
		cout << endl;
	}
}
}

void SevenCardStud::dealeachturn(int numcardsneeded){
	for (int i = 0; i < numcardsneeded; ++i){
		unsigned int pos = (i + 1 + dealpos) % (vsp.size()); //calculates current iterating position
		if (!(*vsp[pos]).folded){
			(*vsp[pos]).hand << deckMain;
		}
	}

}