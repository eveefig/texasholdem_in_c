#include "stdafx.h"
#include "Card.h"
#include "DeckandHand.h"
#include "pokerranks.h"
#include "Player.h"
#include "Game.h"
#include <sstream>
#include <algorithm>
#include <string>
#include <vector>
#include<stdbool.h>
#include <fstream>
#include <iostream>
#include <random>       
#include <chrono>
#include <memory>
using namespace std;
class Deck;
class Hand;
struct Player;
//Default constructor for a deck, initializes vector v.
Deck::Deck()
:v(0){
}

//constructor for a deck that takes a filename and passes it to load to check
// for valid card strings.
Deck::Deck(const char * &file)
: v(){
	Deck::load(file);
}

Card::rank charToRank(char & b);
Card::suit charToSuit(char & c);

//Load method checks for valid card strings and pushes them onto the deck.
//identical to my parse method in previous labs.
int Deck::load(const char * &file){
	ifstream ifs;
	string str;
	ifs.open(file);
	if (!ifs.is_open()){
		throw errorCode::FILE_CANT_OPEN;
		return 1;
	}
	while (ifs >> str){  //While the ifs stream can still insert into the string
		if (checkString2(str)){
			char & r1 = str[0];
			char st = tolower(str[1]);
			char & s1 = st;
			Card::rank r = charToRank(r1);
			Card::suit c = charToSuit(s1);
			Card car;
			car.r_ = r;
			car.s_ = c;
			v.push_back(car);
		}
		if (checkString3(str)){
			char & r1 = str[0];
			char st = tolower(str[2]);
			char & s1 = st;
			Card::rank r = charToRank(r1);
			Card::suit c = charToSuit(s1);
			Card car;
			car.r_ = r;
			car.s_ = c;
			v.push_back(car);
		}
	}
	if (v.size() == 0){
		throw errorCode::LOAD_EMPTY;
		return 3;
	}
	return 0;
}

void Deck::add_card(Card &c){
	v.push_back(c);
}
//Method to randomly shuffle the cards of a deck.
int Deck::shuffle(){
	//set seed based on the system clock
	unsigned int seed = (unsigned int)std::chrono::system_clock::now().time_since_epoch().count();
	// shuffle the vector of the deck using a random number engine with a seed based on the system clock.
	std::shuffle(Deck::v.begin(), Deck::v.end(), std::default_random_engine(seed));
	return 0;
}
//Get the size of a deck
const int Deck::size(){
	return Deck::v.size();
}
//method for printing a deck to the output stream.
ostream& operator<<(ostream& os, const Deck& d){

	os << (printV(d.v));
	return os;
}
//default constructor. Initializes the vector of a Hand.
Hand::Hand(){
	Hand::li;
}
//Copy constructor for a Hand.
//I did not end up using this method because I could not think of a time when
// it would be beneficial.
Hand::Hand(const Hand & h){
	li = h.li;
}
Hand::Hand(const vector<Card>& car){
	li = car;
}
void Hand::operator =(const Hand &h){
	if (this != &h){
		li.clear();
		li = h.li;
	}
}
//Get the size of a hand.
const int Hand::size(){
	return Hand::li.size();
}

std::string getCardSuit(Card & c);
std::string getCardRank(Card & c);
//Method for returning c++ style string of a hand.
//This method was not used for my program.
const std::string Hand::asString(){
	string cardString;
	for (unsigned int i = 0; i < li.size(); ++i){
		Card &car = li[i];
		cardString += getCardRank(car) + getCardSuit(car);
	}
	return cardString;
}
//Method for printing the contents of a hand
ostream& operator<<(ostream& os, const Hand& h){
	os << printV(h.li);
	return os;
}
//A method for inserting the last Card in a deck into a hand, then deleting the card from the deck.
//Hands are kept in sorted order by sorting after each insertion.
Hand& operator<<(Hand& h, Deck& d){
	if (d.size() == 0){
		throw errorCode::INSERT_TO_EMPTY_DECK;
	}

	if (d.size() > 0){
		h.li.push_back(d.v.back()); //push back the last element of the deck into the Hand
		//sort(h.li.begin(), h.li.end());
		d.v.pop_back(); //Delete the last element in the Deck
	}
	return h;
}

const int Hand::getHandRank(){
	int handRank = pokerRank(li);
	return handRank;
}
//A method to get the vector of cards associated with a Hand.
const std::vector<Card> Hand::getVec(){
	return Hand::li;
}
Card Hand::operator[](size_t s){
	return li[s];
}

void Hand::remove_card(size_t t){
	if (li.size() > t){
		li.erase(li.begin() + t);
	}
	else{
		throw errorCode::CARD_DNE;
	}
}




/*Method for ranking each poker hand and breaking ties between the same rank
This method is of type bool so that we can use it as a third parameter in the STL sort
algorithm to sort a vector of Hand objects.*/
bool poker_Rank(Hand& h1, Hand& h2){
	//We compare each Hand's int poker Rank value.
	// values range from 1-9 and represent the 9 different ranks a hand can have.
	vector<Card> v1 = h1.getVec();
	sort(v1.begin(), v1.end());
	vector<Card> v2 = h2.getVec();
	sort(v2.begin(), v2.end());
	int x = pokerRank(v1);
	int y = pokerRank(v2);
	if (x != y){ //As long as the two hands don't have the same rank we can return the higher ranked hand
		return x < y;
	}
	else{
		
		//If both hands are a 4 of a kind or if both hands are a 3 of a kind.
		if (x == 8 || x == 4){
			//in either case, we sort by the higher 4 or 3 of a kind. 1 of the matching cards has to be in the 
			// middle position, because the hand is sorted. Return the higher of the 3rd position
			return v1[2] < v2[2];
		}
		//Both hands are a full house.
		else if (x == 7){
			if (getCardRank(v1[2]) == getCardRank(v2[2])){
				return highCard(v1, v2); //If both hands have the same 3 of a kind, return the higher two of a kind.
			}
			return v1[2] < v2[2]; //return the higher 3 of a kind.
		}
		//Both hands are a Two pair.
		else if (x == 3){
			//In two pairs, one card of the larger pair must be in position 3
			// and a card of the smaller pair must be in position 1, in a sorted hand.
			if (getCardRank(v1[3]) == getCardRank(v2[3])){
				if (getCardRank(v1[1]) == getCardRank(v2[1])){

					return highCard(v1, v2);   //If both pairs in both hands are the same, return the highest 5th card
				}

				else //If the largest pair is equal but the smallest pair is not
					// return the larger of the smallest pair
				{
					return v1[1] < v2[1];
				}
			}
			else //If the larger pair is not equal, return the larger pair
			{
				return v1[3] < v2[3];
			}
		}

		//Both hands are a single pair
		else if (x == 2){
			Card a;
			Card b;
			/*Because I could not get adjacent_find to work properly,
			I had to find the pair by brute force, checking each card in a hand
			against the next card. Set a temporary card for the pair in each vector
			*/
			if (getCardRank(v1[0]) == getCardRank(v1[1])){
				a = v1[0];
			}
			if (getCardRank(v1[1]) == getCardRank(v1[2])){
				a = v1[1];
			}
			if (getCardRank(v1[2]) == getCardRank(v1[3])){
				a = v1[2];
			}
			if (getCardRank(v1[3]) == getCardRank(v1[4])){
				a = v1[3];
			}
			if (getCardRank(v2[0]) == getCardRank(v2[1])){
				b = v2[0];
			}
			if (getCardRank(v2[1]) == getCardRank(v2[2])){
				b = v2[1];
			}
			if (getCardRank(v2[2]) == getCardRank(v2[3])){
				b = v2[2];
			}
			if (getCardRank(v2[3]) == getCardRank(v2[4])){
				b = v2[3];
			}
			//If both hands have the same pair we return the high card
			if (getCardRank(a) == getCardRank(b)){
				return highCard(v1, v2);
			}

			return (a < b); //return which pair is higher
		}
		/* If both hands are a straight flush, flush, straight, or have no rank,
		we can simply rank the hands by high card
		*/
		else{
			return highCard(v1, v2);
		}
	}
}

bool poker_Rank_p(shared_ptr<Player> p1, shared_ptr<Player> p2){
	sort((*p1).hand.li.begin(), (*p1).hand.li.end());
	sort((*p2).hand.li.begin(), (*p2).hand.li.end());
	if (p1 == nullptr){
		return false;
	}
	else if (p2 == nullptr){
		return true;
	}
	else {
		return poker_Rank((*p1).hand, (*p2).hand);
	}
}
/*A method for sorting two hands by high card. iterate through each sorted hand
If v1 is greater at any point than the same card in v2, we know that v1 has the highCard.
This only works because each hand is sorted. We keep a side count of the int value of each card's rank
If v1 is never larger than v2 but their int rank values are equal, we know that v1 and v2 have the same
exact cards.
Our program does not specify how to handle this, so we throw an exception that the hands could not be sorted and return the
unsorted hands.
*/
const bool highCard(std::vector<Card>& v1, std::vector<Card> &v2){
	if (getCardRank(v1[4]) == getCardRank(v2[4])){
		if (getCardRank(v1[3]) == getCardRank(v2[3])){
			if (getCardRank(v1[2]) == getCardRank(v2[2])){
				if (getCardRank(v1[1]) == getCardRank(v2[1])){
					if (getCardRank(v1[0]) == getCardRank(v2[0])){
						return v1[4].s_ < v2[4].s_;
						throw errorCode::SAME_HAND;
					}
					else{
						return (v1[0] < v2[0]);
					}
				}
				else{
					return (v1[1] < v2[1]);
				}
			}
			else{
				return (v1[2] < v2[2]);
			}
		}
		else{
			return (v1[3] < v2[3]);
		}
	}
	else{
		return (v1[4] < v2[4]);
	}

}

