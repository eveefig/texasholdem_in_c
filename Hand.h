
#ifndef HAND_H
#define HAND_H

#include "Card.h"
#include <string>
#include <vector>
#include <stdbool.h>
#include <memory>

class Hand{
	std::vector<Card> li;

	//insertion operator to print out a Hand, which contains a vector<Card>
	friend std::ostream& operator<<(std::ostream& os, const Hand& h);
	//insertion operator to insert the last card in a Deck into the vector of a Hand
	friend Hand& operator<<(Hand& h, Deck& d);

public:
	Hand();
	Hand(const Hand &);
	void operator =(const Hand & h);
	const int size();
	const bool & Hand::operator==(const Hand &rightH)const;
	const bool & Hand::operator<(const Hand &)const;
	const std::string Hand::asString();

	const int Hand::getHandRank();
	const std::vector<Card> Hand::getVec();

	Card operator[](size_t);

	void Hand::remove_card(size_t);
	std::string Hand::getOddCard(Hand & h);
	std::string Hand::getOddThree(Hand & h);
	std::string Hand::getOddPair(Hand& h);
};

bool poker_Rank(Hand& h1, Hand& h2);
bool poker_Rank_p(std::shared_ptr<Player> p1, std::shared_ptr<Player> p2);
const bool highCard(std::vector<Card> & v1, std::vector<Card> & v2);

#endif