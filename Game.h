#ifndef GAME_H
#define GAME_H

#include "Player.h"
#include <string>
#include <memory>

class Game {
	friend int play();
protected:
	static std::shared_ptr<Game> gsp;
	Deck deckMain;
	std::vector<std::shared_ptr<Player>> vsp;
	int hand_size;

public:
	Game();

	static std::shared_ptr<Game> instance();
	static void start_game(const std::string &);
	void stop_game();

	void add_player(const std::string & s);
	std::shared_ptr<Player> find_player(const std::string & f);

	void leave();
	void join();

	void betHandler();

	int virtual before_round() = 0;
	int virtual round() = 0;
	int virtual after_round() = 0;

	int virtual betting() = 0;
	int virtual betHandler(const std::string &ans, Player &, unsigned int &) = 0;

	virtual ~Game();

	void virtual print(unsigned int yourpos) = 0;
	void checkPlayerChips();


	unsigned int numPlayers;
	unsigned int numFolded;  //Keeps track of the number of players that have folded so far in the game
	unsigned int pot_of_chips;
};


#endif
