#include "stdafx.h"
#include "FiveCardDraw.h"
#include "Errors.h"
#include "Card.h"
#include "DeckandHand.h"
#include <algorithm>
#include <string>
#include <istream>
#include <iostream>
#include <vector>
#include <ostream> 
#include <fstream>
#include <sstream>
using namespace std;

//Constructor initializes the dealer position to zero and loads 52 cards into the deck
FiveCardDraw::FiveCardDraw() {
	hand_size = 5;
	numFolded = 0;
}

//before_turn asks each player if they would like to remove any cards from their hand
int FiveCardDraw::before_turn(Player & pl){
	if (!pl.folded){
		cout << "\nName: " << pl.name << ", Hand: " << pl.hand << endl;
		cout << "If you would like to discard any card(s) please give the position of the card(s) you would like to discard separated by spaces. Otherwise, press return." << endl;
		cin.sync();
		unsigned int i;
		Card c;
		string s;
		stringstream ss;

		getline(cin, s);
		ss << s;

		vector<unsigned int> store;
		while (ss >> i){
			store.push_back(i - 1);
		}
		//sort all of the inputs from largest to smallest so we can avoid index out of bounds errors
		sort(store.begin(), store.end());
		auto it = unique(store.begin(), store.end());
		store.erase(it, store.end());
		reverse(store.begin(), store.end());
		//if the player asks to remove more cards than are in the hand
		if (store.size() > pl.hand.getVec().size()){
			return (int)errorCode::NOT_ENOUGH_CARDS_IN_HAND;
		}
		for (unsigned int i = 0; i < store.size(); ++i){
			//if the player asks to remove a card less than 1 or more than 5
			if (store[i] >= pl.hand.getVec().size()){
				return (int)errorCode::INDEX_OUT_OF_BOUNDS;
			}
			else{
				//remove card from hand and add to discard deck
				c = pl.hand.getVec()[store[i]];
				pl.hand.remove_card(store[i]);
				
				discard.add_card(c);
			}
		}
		cout << "\nName: " << pl.name << ", Current Hand: " << pl.hand << endl;
		cin.clear();
	}
	return 0;
}

//turn deals a player enough cards so they have 5 cards in their hand
int FiveCardDraw::turn(Player & pl){
	if (!pl.folded){
		//if a player needs cards and the main deck has enough to give
		if (deckMain.size() >= (hand_size - pl.hand.size())){
			while (pl.hand.size() < hand_size){
				pl.hand << deckMain;
			}
		}
		//if the main deck has some cards, but not enough to put 5 cards in a hand
		else{
			//use up all cards in main deck first
			while (deckMain.size()>0){
				pl.hand << deckMain;
			}
			//continue to deal from discard pile
			discard.shuffle();
			while (pl.hand.size() < hand_size){
				try{
					pl.hand << discard;
				}
				catch (errorCode &e){
					cout << errorString(e) << endl;
					return (int)e;
				}
			}
		}
	}
	return 0;
}

//after_turn prints out the name and hand of a player
int  FiveCardDraw::after_turn(Player & pl){
	if (!pl.folded){
		cout << "Name: " << pl.name << " Hand: " << pl.hand.asString() << endl;
	}

	return 0;
}
int FiveCardDraw::call_before_turn(){
	//Calls before turn method for each player, starting with 1 past dealer position.
	//If before turn does not return zero, we cout an error and try the method again.
	for (unsigned int i = 0; i < vsp.size(); ++i){
		unsigned int pos = (i + 1 + dealpos) % vsp.size(); //calculates current iterating position
		if (!(*vsp[pos]).folded){
			unsigned int f = before_turn(*vsp[pos]);
			while (f != 0){
				cout << "Outside the range of cards. Please try again: " << endl;
				f = before_turn(*vsp[pos]);
			}
		}
	}
	return 0;
}
//before_round shuffles the main deck and deals them to the players, then calls before_turn
int  FiveCardDraw::before_round(){
	deckMain.shuffle();
	ante();
	//Deals Cards to each player
	if ((unsigned int)deckMain.size() >= (unsigned int)vsp.size() * hand_size){

		int numcardsneeded = hand_size * vsp.size();
		for (int i = 0; i < numcardsneeded; ++i){
			unsigned int pos = (i + 1 + dealpos) % vsp.size(); //calculates current iterating position
			(*vsp[pos]).hand << deckMain;
		}
		
		//Once all players have their cards we want to start a round of betting.
		int v = betting();
			while (v != 0){
				v = betting();
			}
		call_before_turn();
	}
	else{
		//deck does not have enough cards to give each player 5 cards
		throw errorCode::NOT_ENOUGH_CARDS;
	}
	return 0;
}

//round calls turn and after_turn for each player, starting with the person after the dealer
int FiveCardDraw::round() {
	for (unsigned int i = 0; i < vsp.size(); ++i){
		int pos = (i + 1 + dealpos) % vsp.size(); //calculates current iterating position
		try{
			turn(*vsp[pos]);
		}
		catch (errorCode &e){
			cout << errorString(e) << endl;
			return (int)e;
		}
	}

	int v = betting();
	while (v != 0){
		v = betting();
	}

	//after_turn(*vsp[pos]);
	return 0;
}

/*after_round determines who wins and adds 1 to their win count.  It adds 1 to everyone else's lose
count.  It then takes the cards from the discard deck and puts them in the main deck.  It then takes
the cards from everyone's hands and puts them in the main deck.  It then asks if you want to remove
or add any players.*/
int FiveCardDraw::after_round(){
	vector<shared_ptr<Player>> vspcopy = vsp;
	//sort by poker rank
	sort(vspcopy.begin(), vspcopy.end(), poker_Rank_p);
	updateScores(vspcopy);
	moveCards(vsp);

	checkPlayerChips();

	leave();
	join();
	int newpos = (dealpos + 1) % vsp.size();
	dealpos = newpos;
	return 0;
}


void FiveCardDraw::print(unsigned int yourpos) {
	cout << "\n" << (*vsp[yourpos]).name << ", this is your current hand: " << (*vsp[yourpos]).hand << endl;
	cin.clear();
}
