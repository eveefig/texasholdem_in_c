#ifndef DECKANDHAND_H
#define DECKANDHAND_H

#include <string>
#include <vector>
#include <stdbool.h>
#include <memory>

class Hand;
struct Card;
struct Player;
class FiveCardDraw;

class Deck{
	friend Hand& operator<<(Hand& h, Deck& d);
	std::vector<Card> v;

public:
	Deck();
	Deck(const char * &file);
	int Deck::load(const char * &file);
	void Deck::add_card(Card &c);
	int shuffle();
	const int size();

private:
	friend std::ostream& operator<<(std::ostream& os, const Deck& d);
};

class Hand{
	std::vector<Card> li;

	//insertion operator to print out a Hand, which contains a vector<Card>
	friend std::ostream& operator<<(std::ostream& os, const Hand& h);
	//insertion operator to insert the last card in a Deck into the vector of a Hand
	friend Hand& operator<<(Hand& h, Deck& d);
	friend bool poker_Rank_p(std::shared_ptr<Player> p1, std::shared_ptr<Player> p2);

public:
	Hand();
	Hand(const Hand &);
	Hand(const std::vector<Card>&);
	void operator =(const Hand & h);
	const int size();
	const bool & Hand::operator==(const Hand &rightH)const;
	const bool & Hand::operator<(const Hand &)const;
	const std::string Hand::asString();

	const int Hand::getHandRank();
	const std::vector<Card> Hand::getVec();

	Card operator[](size_t);

	void Hand::remove_card(size_t);
};
const bool nineHands(Deck & d);

const bool highCard(std::vector<Card>& v1, std::vector<Card> &v2);
bool poker_Rank(Hand& h1, Hand& h2);
bool poker_Rank_p(std::shared_ptr<Player> p1, std::shared_ptr<Player> p2);

#endif
