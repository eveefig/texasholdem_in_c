#include "stdafx.h"
#include "pokerranks.h"
#include "Card.h"
#include "DeckandHand.h"
#include <algorithm>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <stdbool.h>
#include <iostream>
using namespace std;

//We define a default constructor for Card.
// the default value is an Ace of Clubs, though for our program the default value is never used.
Card::Card()
:s_(suit::clubs), r_(rank::ace){}

//for our non-default constructor we take in a suit and rank and set the card equal to them.
Card::Card(suit s, rank r)
: s_(s), r_(r){}

//This method allows us to take a char and convert it into the appropriate suit by reading the value of the char.
// if the suit is not found, an error message is thrown.
// We convert the char to lowercase to standardize the chars.
Card::suit charToSuit(char & b){
	Card::suit s;
	char c = tolower(b);

	if (c == 'd'){ return s = Card::suit::diamonds; }
	if (c == 'c'){ return s = Card::suit::clubs; }
	if (c == 'f'){ return s = Card::suit::spades; }
	else { return s = Card::suit::hearts; }

	
}

//Similar to the previous method, a reference to a char is fed in and the appropriate rank is returned.

Card::rank charToRank(char & c){
	Card::rank r;
	c = tolower(c);
	if (c == 'm'){ return r = Card::rank::ace; }
	if (c == 'a'){ return r = Card::rank::two; }
	if (c == 'b'){ return r = Card::rank::three; }
	if (c == 'c'){ return r = Card::rank::four; }
	if (c == 'd'){ return r = Card::rank::five; }
	if (c == 'e'){ return r = Card::rank::six; }
	if (c == 'f'){ return r = Card::rank::seven; }
	if (c == 'g'){ return r = Card::rank::eight; }
	if (c == 'h'){ return r = Card::rank::nine; }
	if (c == 'i'){ return r = Card::rank::ten; }
	if (c == 'j'){ return r = Card::rank::jack; }
	if (c == 'k'){ return r = Card::rank::queen; }
	else  { return r = Card::rank::king; }
}
//getRank and getSuit is used for printing the rank and suit of each card passed through.
//We use a separate switch for rank and then suit.
string getRank(Card & c){

	Card::rank r = c.r_;
	switch (r){
	case Card::rank::two: return "2"; break;
	case Card::rank::three: return "3"; break;
	case Card::rank::four:  return "4"; break;
	case Card::rank::five: return  "5"; break;
	case Card::rank::six:  return"6"; break;
	case Card::rank::seven: return "7"; break;
	case Card::rank::eight: return "8"; break;
	case Card::rank::nine: return "9"; break;
	case Card::rank::ten: return"10"; break;
	case Card::rank::jack: return "j"; break;
	case Card::rank::queen: return "q"; break;
	case Card::rank::king: return "k"; break;
	}
	return "a";
}
string getSuit(Card & c){
	Card::suit s = c.s_;
	switch (s){
	case Card::suit::clubs:  return"c "; break;
	case Card::suit::diamonds: return "d "; break;
	case Card::suit::spades: return "s "; break;
	}
	return "h ";
}
//Our print method iterates through the vector v and prints out the correct rank and suit by calling
// cardToChar.
string printV(const vector<Card>  & v){
	string str = "";
	vector<Card> hand = v;
	if (v.size() == 0){
//		throw errorCode::VECTOR_IS_EMPTY;
		cout << "Vector is empty. No valid cards can be printed." << endl;
		//return 2;		
	}
	else{
		for (size_t i = 0; i < v.size(); i++){
			Card car = hand[i];
			str += getRank(car);
			str += getSuit(car);
		}
	}
	return str;
}

string getCardSuit(Card & c){
	Card::suit s = c.s_;
	switch (s){
	case Card::suit::clubs: return "c "; break;
	case Card::suit::diamonds: return "d "; break;
	case Card::suit::spades:  return "s "; break;
	case Card::suit::hearts: return  "h "; break;
	}
	throw errorCode::RANK_OR_SUIT;

}
string getCardRank(Card & c){

	Card::rank r = c.r_;
	switch (r){
	case Card::rank::two: return "2"; break;
	case Card::rank::three: return "3"; break;
	case Card::rank::four:return "4"; break;
	case Card::rank::five: return "5"; break;
	case Card::rank::six: return "6"; break;
	case Card::rank::seven: return "7"; break;
	case Card::rank::eight: return "8"; break;
	case Card::rank::nine:return "9"; break;
	case Card::rank::ten: return "10"; break;
	case Card::rank::jack: return "j"; break;
	case Card::rank::queen: return "q"; break;
	case Card::rank::king: return "k"; break;
	case Card::rank::ace: return "a"; break;
	}
	throw errorCode::RANK_OR_SUIT;
}

//checkString is a method to make sure the card is in a Valid card definition string
// we have 2 methods, one to check if the card has a rank 10 and the other for all other values.
//If the string is valid we return true.
bool checkString3(string str){
	char r = tolower(str[0]);
	char & r1 = r;
	if (str.size() == 3){ //the only card that does not have size 2 is a 10.
		char & r2 = str[1];
		if (r1 == '1' && r2 == '0'){
			char s = tolower(str[2]);
			char & s1 = s;
			if (s1 == 's' || s1 == 'h' || s1 == 'd' || s1 == 'c'){
				return true;
			}
		}
	}
	return false;
}
bool checkString2(string str){
	char r = tolower(str[0]);
	char & r1 = r;
	if (str.size() == 2){

		if (r1 != '1'){
			if (r1 == 'a' || r1 == 'j' || r1 == 'k' || r1 == 'q' || isdigit(r1)){

				char st = tolower(str[1]);
				char & s1 = st;
				if (s1 == 's' || s1 == 'h' || s1 == 'd' || s1 == 'c'){
					return true;
				}
			}
		}
	}
	return false;
}
//Method for converting an error code to a string.
//Gets the int value associated with an error code and returns a string from an array
// indexed by that int.
string errorString(errorCode e){
	return error_names[e];
}