// Lab2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Card.h"
#include "helperFunctions.h"
#include "pokerranks.h"
#include "DeckandHand.h"
#include "Game.h"
#include"FiveCardDraw.h"

#include "Player.h"
#include <string>
#include <algorithm>
#include <vector>
#include <iostream>
using namespace std;

int main(int argc, char* argv[]){

	try{
		//make sure you have enough players to play the game
		if (argc < 4){
			cout << errorString(YOU_GIANT_DUMMY) << endl;
			return (int)errorCode::YOU_GIANT_DUMMY;
		}
		//make sure you don't have too many players for a deck of 52 cards
		if (argc > 12){
			cout << errorString(YOU_GIANT_DUMMY) << endl;
			return (int)errorCode::YOU_GIANT_DUMMY;
		}
	}
	catch (errorCode &e){
		cout << errorString(e) << endl;
	}
	string gameName = argv[1];
	try{
		//start the game
		Game::start_game(gameName);
	}
	catch (errorCode &e){
		cout << errorString(e) << endl;
		return (int)e;
	}
	shared_ptr<Game> g;
	try{
		//get a pointer to the game
		g = Game::instance();
	}
	catch (errorCode &e){
		cout << errorString(e) << endl;
		(*g).stop_game();
		return (int)e;
	}
	//iterate through the inputted players and add them to the game
	for (int i = 2; i < argc; ++i){
		auto playerName = argv[i];
		try{
			(*g).add_player(playerName);
		}
		catch (errorCode &e){
			cout << errorString(e) << endl;
			(*g).stop_game();
			return (int)e;
		}
	}
	//Continue going through before_round, round, and after_round while you have at least 2 players
	while ((*g).numPlayers > 1){
		try{
			(*g).before_round();
		}
		catch (errorCode e){
			cout << errorString(e) << endl;
		}
		try{
			(*g).round();
		}
		catch (errorCode e){
			cout << errorString(e) << endl;
			(*g).stop_game();
			return (int)e;

		}
		try{
			(*g).after_round();
		}
		catch (errorCode e){
			cout << errorString(e) << endl;
			(*g).stop_game();
			return (int)e;
		}
		//ignore the first character in a round because it is /n
		cin.ignore();

	}
	//if you remove too many players, you can no longer play
	if ((*g).numPlayers < 2){
		cout << errorString(NOT_ENOUGH_PLAYERS) << endl;
		return (int)errorCode::NOT_ENOUGH_PLAYERS;
	}
	//end the game
	(*g).stop_game();
	return 0;
}

