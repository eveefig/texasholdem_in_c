#ifndef ERRORS_H
#define ERRORS_H

#include <string>

enum errorCode{
	SUCCESS,
	USAGE,
	FILE_CANT_OPEN,
	VECTOR_IS_EMPTY,
	LOAD_EMPTY,
	SAME_HAND,
	RANK_OR_SUIT,
	NOT_ENOUGH_CARDS,
	CARD_DNE,
	INSTANCE_NOT_AVAILABLE,
	GAME_ALREADY_STARTED,
	UNKNOWN_GAME,
	NO_GAME_IN_PROGRESS,
	ALREADY_PLAYING,
	INSERT_TO_EMPTY_DECK,
	INDEX_OUT_OF_BOUNDS,
	NOT_ENOUGH_PLAYERS,
	NOT_ENOUGH_CARDS_IN_HAND,
	INVALID_RESPONSE,
	NOT_ENOUGH_CHIPS
};

namespace {
	const char *error_names[] = {
		"Success!",
		"Error: Incorrect number of arguments",
		"Error: The file supplied could not be opened!",
		"Error: Vector was empty. No valid cards can be printed!",
		"Error: No cards could be loaded into the deck!",
		"Error: Hands can not be sorted: Two hands are exactly the same...Someone might be cheating. Below are the unsorted Hands:",
		"Error: Either the suit or rank could not be read from the Card!",
		"Error: There were not enough cards in the deck to give each player 5 cards. A Deck has 52 cards.",
		"Error: The card you are trying to remove does not exist!",
		"Error: Could not return instance of Game!",
		"Error: There is a game already in progress!",
		"Error: The game you tried to start is not recognized. Your game name must contain 'FiveCardDraw' or 'SevenCardStud'!",
		"Error: Stop game was called but there is no game currently in progress!",
		"Error: There is already a player with this name in the game!",
		"Error: Tried inserting cards from an empty deck!",
		"Error: You are trying to remove a card outside of the size of your hand!",
		"Error: You are trying to play with fewer than 2 players!",
		"Error: You have no more cards to remove!",
		"Error: Sorry, the response you gave was invalid. Please try again.\n",
		"Error: Sorry, you don't have enough chips. Please try again. \n"
	};
}
std::string errorString(errorCode e);


# endif 