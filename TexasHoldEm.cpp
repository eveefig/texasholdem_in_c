#include "stdafx.h"
#include "TexasHoldEm.h"
#include "Errors.h"
#include "Card.h"
#include "DeckandHand.h"
#include "pokerranks.h"
#include <memory>
#include <algorithm>
#include <string>
#include <istream>
#include <iostream>
#include <vector>
#include <ostream>
#include <fstream>
#include <sstream>

using namespace std;

class IntermediateGame;
class Game;
struct Card;
class Hand;
std::string getCardSuit(Card & c);
std::string getCardRank(Card & c);

void ante();
void dealdeck();
string getCardSuit(Card & c);
string getCardRank(Card & c);


TexasHoldEm::TexasHoldEm()
{
	hand_size = 2;
	numFolded = 0;
}

int TexasHoldEm::before_round(){
	deckMain.shuffle();
	ante();
	return 0;
}


int TexasHoldEm::round(){
	first_turn(); //first round deals 3 cards
	second_turn();
	other_turns(); // all other rounds deal just one
	other_turns();
	return 0;
}

int TexasHoldEm::after_round(){
	for (unsigned int i = 0; i < vsp.size(); ++i){
		vector<Card> totalHand;
		vector<Card> playerCards = (*vsp[i]).hand.getVec();
		for (auto j = playerCards.begin(); j != playerCards.end(); ++j) {
			allCards.push_back(*j);
			totalHand.push_back(*j);
		}
		vector<Card> otherCards = communityCards.getVec();
		for (auto j = otherCards.begin(); j != otherCards.end(); ++j) {
			totalHand.push_back(*j);
		}
		vector<Card> temp = seven_choose_five(totalHand);
		sort(temp.begin(), temp.end());
		Hand h = Hand(temp);
		(*(vsp)[i]).hand = h;
	}
	sort(vsp.begin(), vsp.end(), poker_Rank_p);
	updateScores(vsp);
	for (unsigned int i = 0; i < vsp.size(); ++i){
		(*vsp[i]).hand = Hand();
	}
	for (auto it = allCards.begin(); it != allCards.end(); ++it){
		deckMain.add_card(*it);
	}
	allCards.clear();
	communityCards = Hand();
	checkPlayerChips();


	leave();
	join();
	int newpos = (dealpos + 1) % vsp.size();
	dealpos = newpos;
	return 0;
}

int TexasHoldEm::first_turn(){
	//deck has enough cards for everyone to get 7, deal 2 cards to each player
	int firstturncards = (numPlayers - numFolded) * 2;
	dealeachturn(firstturncards); //deal first round
	//Once all players have their cards we want to start a round of betting.

	int v = betting();
	while (v != 0){
		v = betting();
	}
	return 0;
}

int TexasHoldEm::second_turn(){
	//deck has enough cards for everyone to get 7, deal 2 cards to each player
	int secondturncards = 3;
	for (int i = 0; i < secondturncards; ++i) {
		communityCards << deckMain;
		allCards.push_back(communityCards[i]);
	}
	//Once all 3 cards are dealt we want to start a round of betting.

	int v = betting();
	while (v != 0){
		v = betting();
	}
	return 0;
}

int TexasHoldEm::other_turns(){
	communityCards << deckMain;
	//Once all players have their cards we want to start a round of betting.
	allCards.push_back(communityCards[communityCards.size()-1]);

	int v = betting();
	while (v != 0){
		v = betting();
	}
	return 0;
}

void TexasHoldEm::print(unsigned int yourpos){

	cout << "\n" << (*vsp[yourpos]).name << ", this is your current hand: " << (*vsp[yourpos]).hand << endl;	
			cout << "Community cards: " << communityCards;
			cout << endl;		
}

void TexasHoldEm::dealeachturn(int numcardsneeded){
	for (int i = 0; i < numcardsneeded; ++i){
		unsigned int pos = (i + 1 + dealpos) % (vsp.size()); //calculates current iterating position
		if (!(*vsp[pos]).folded){
			(*vsp[pos]).hand << deckMain;
		}
	}
}

