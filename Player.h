#ifndef PLAYER_H
#define PLAYER_H

#include "stdafx.h"
#include "DeckandHand.h"
#include <string>

#include "Errors.h"
struct Player{
	Player();
	Player(char*);

	std::string name;
	Hand hand;
	unsigned int won;
	unsigned int lost;
	unsigned int chips;
	unsigned int bet;
	bool folded;
};

std::ostream & operator<<(std::ostream &, const Player &);

#endif /* PLAYER_H */
