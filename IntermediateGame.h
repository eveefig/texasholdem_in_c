#ifndef INTERMEDIATEGAME_H
#define INTERMEDIATEGAME_H

#include "Game.h"
#include "Player.h"
#include <string>

class Game;
class IntermediateGame : public Game {

protected:
	size_t dealpos;
	Deck discard;

public:	
	IntermediateGame();

	void ante();
	void dealdeck();

	int virtual betting();
	int virtual betHandler(const std::string &ans, Player &, unsigned int &);

	void updateScores(std::vector<std::shared_ptr<Player>> &);
	void moveCards(std::vector<std::shared_ptr<Player>> &);

	std::vector<Card> seven_choose_five(std::vector<Card>);

};

#endif