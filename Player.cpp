#include "stdafx.h"
#include "Player.h"
#include "Card.h"
#include "DeckandHand.h"
#include <string>
#include <fstream>
using namespace std;

//default constructor initializes everything to default values
Player::Player(){};

/*constructor gives a player his/her name based on the char * input. Because it is a new player, the
win and loss counters are initialized to zero.We begin by saying this player is not a robot*/
Player::Player(char * n)
:name(n), hand(), won(0), lost(0), chips(1), bet(0), folded(false){
	ifstream ifs;
	ifs.open(n);

	//if the player already has a file, read in his/her current win and loss count
	if (ifs.is_open()){
		ifs >> name;
		ifs >> won;
		ifs >> lost;
		ifs >> chips;
	}
	//otherwise, they've never played before and now need to start from zero
	else{
		won = 0;
		lost = 0;
		chips = 1;
		bet = 0;
		folded = false;
	}
}

ostream & operator<<(ostream & o, const Player & p) {
	o << p.name << " " << p.won << " " << p.lost << " " << p.chips;
	return o;
}

