#ifndef POKERRANKS_H
#define POKERRANKS_H


#include "Errors.h"
#include <string>
#include<vector>
#include "Card.h"
int pokerRank(const std::vector<Card> & v);
bool isFlush(std::vector<Card>);
bool isStraight(const std::vector<Card> &v);
bool isFullHouse(const std::vector<Card> &v);
bool isThreeK(const std::vector<Card> &v);

//since the number of pairs can be 0, 1, or 2, we cannot use a type bool.
int numPairs(const std::vector<Card> &hand, int & p);


#endif
