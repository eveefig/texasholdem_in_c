#include "stdafx.h"
#include "Errors.h"
#include "Card.h"
#include "Player.h"
#include "IntermediateGame.h"
#include "pokerranks.h"
#include <algorithm>
#include <memory>
#include <string>
#include <vector>
#include <istream>
#include <iostream>
#include <ostream>
#include <fstream>
#include <sstream>
using namespace std;

Card::rank charToRank(char & c);
Card::suit charToSuit(char & b);

std::string getCardSuit(Card & c);
std::string getCardRank(Card & c);

//Constructor initializes the dealer position to zero and loads 52 cards into the deck
IntermediateGame::IntermediateGame()
:discard(), dealpos(0){
	dealdeck();
	pot_of_chips = 0;
}

//Before the game we want to deduct one chip from each player
// and add it to the pot of chipt. An ante.
void IntermediateGame::ante(){
	for (unsigned int k = 0; k < numPlayers; ++k){
		int tempChips = (*vsp[k]).chips;
		if (tempChips>=0){
			--(*vsp[k]).chips;
			++pot_of_chips;
		}
	}
}

//Deal the deck
void IntermediateGame::dealdeck() {
	for (char r = (char)Card::rank::two; r <= (char)Card::rank::ace; ++r) {
		for (char s = (char)Card::suit::clubs; s <= (char)Card::suit::spades; ++s) {
			Card c;
			c.r_ = charToRank(r);
			c.s_ = charToSuit(s);
			deckMain.add_card(c);
		}
	}
}

//A helper method to handle a player's answer during a round of betting.
// This is only called if there is a bet during the round. 
// Handles a call, raise, or fold. Updates pot_round and pl.chips, pl.bet
int IntermediateGame::betHandler(const string &ans, Player & pl, unsigned int &chips_to_call){

	if (ans == "fold"){
		pl.folded = true;
		++numFolded;
	}
	else if (ans == "call"){
		if ((chips_to_call-pl.bet) >= pl.chips){ //Current bet is equal to or more than player's chips.
			cout << "Looks like you're going all in!" << endl;

			pl.bet += pl.chips;  //add all of a player's chips to their bet counter
			pl.chips = 0;		//player now has 0 chips. All in.

		}
		else{
			cout << "Bet called" << endl;  //They have enough chips to call
			cin.clear();
			pl.chips = pl.chips - (chips_to_call-pl.bet); //deduct the amount to call the bet.

			pl.bet += chips_to_call-pl.bet;
		}
	}
	else if (ans == "raise"){
		string s;
		cout << "You can raise 1 or 2 chips. Please enter 1 or 2:" << endl;
		cin.sync();
		
		cin >> s;
		if (s == "1"){
			if (pl.chips < ((chips_to_call-pl.bet) + 1)){ //they can't raise by one, not enough chips
				cout << "Sorry, you don't have enough chips for that " << endl;
				throw errorCode::INVALID_RESPONSE;
			 }
			else{
				pl.chips = pl.chips - (chips_to_call-pl.bet) - 1;
				pl.bet =  1 + chips_to_call; //add the amount of chips to call plus 1 to player's bet
				++chips_to_call;  //the amount of chips to call goes up for the next player
			}
		}
		else if (s == "2"){
			if (pl.chips < ((chips_to_call-pl.bet) + 2)){
				cout << "Sorry, you don't have enough chips to raise 2. Please type call raise or fold" << endl;
				throw errorCode::INVALID_RESPONSE;
			}

			else{ //they raise two and have at least 2+chips_to_call chips
				pl.chips = pl.chips - (chips_to_call-pl.bet) - 2;
				pl.bet = chips_to_call + 2;
				chips_to_call += 2;
			}
		}
		else{
			throw errorCode::INVALID_RESPONSE;
		}
	}

	else{
		throw errorCode::INVALID_RESPONSE;
		
	}

	return 0;
}

//A method to handle betting
int IntermediateGame::betting(){
	unsigned int pot_round = 0; //the current pot total for this round
	unsigned int chips_to_call = 0; //The number of chips that a player must bet in order to call
	string ans;   //Players response for betting (raise, fold, call)
	unsigned int numChecked = 0; //int to keep track of how many players have checked so far. If numChecked=numPlayers we know the round of betting is over
	bool a = true;
	while (a){  //while the round of betting isn't over
		for (unsigned int i = 0; i < numPlayers; ++i){  //iterate through every player
			unsigned int pos = (i + 1 + dealpos) % vsp.size();
			Player &thePlayer = (*vsp[pos]); //Current Player doing the betting
			if (!thePlayer.folded){
				print(pos);

				if (thePlayer.chips==0){ //FIXME Need to handle if player runs out of chips
					cout << "You don't have any chips. You're still in the round but you can't bet" << endl;
					++numChecked;
				}
				else{

				if (pot_round == 0){  //No one has bet so far. Only time you can check
					string newBet = "";
					cout << "If you would like to bet please enter either 1 or 2.\nOtherwise type any other key and hit enter to check.\n Your current chip total is " << thePlayer.chips << endl;
					cin >> newBet;
					if (newBet == "1"){
						if (thePlayer.chips > 0){
							--thePlayer.chips;
							++pot_round;
							++thePlayer.bet;
							++chips_to_call;
						}
						else{
							cout << "Sorry, you don't have any chips to bet. :(" << endl;
						}
					}
					else if (newBet == "2"){
						if (thePlayer.chips > 1){
							thePlayer.chips = thePlayer.chips - 2;
							pot_round += 2;
							thePlayer.bet += 2;
							chips_to_call += 2;
						}
						else{
							cout << "Sorry, looks like you have less than 2 chips" << endl;
							return 2;
						}
					}
					else{
						cout << thePlayer.name << " checks." << endl;
						++numChecked;
						if (numChecked == numPlayers-numFolded){
							cout << "Everyone has checked." << endl;
							cin.clear();
							a = false; //set while loop variable to false.
							break; //break out of for loop, return to whatever method called betting().
						}
					}
				}
				else{ //The pot for this round is not zero.

					if (chips_to_call == thePlayer.bet){ //If the player's bet is equal to the amount of chips to call, we know that no one raised and that the round of betting is over.
						cout << "Round of betting is over" << endl;
						cin.clear();
						a = false; //Set while loop variable to false;
						break; //break out of for loop, return to whatever method called betting().
					}				
					cout << "The current bet is: " << (chips_to_call) << ", you need " << chips_to_call - thePlayer.bet << " chips to call.\nYou have " << thePlayer.chips << " chips" << endl;
					cout << "What action would you like to take?\ncall, raise, or fold?" << endl;
					cin.sync();
					cin >> ans;
					int valid = 1;
					pot_round = pot_round - thePlayer.bet;
					while (valid != 0){
						try{
							valid = betHandler(ans, thePlayer, chips_to_call);
						}
						catch (errorCode e){
							cout << errorString(e);
							ans = "";
							cin.clear();
							cin.ignore(10000, '\n');
							cin >> ans;
							valid = 1;
						}
					}
					pot_round += thePlayer.bet;

					if (thePlayer.folded){
						if (numFolded == (numPlayers - 1)){ //everyone but 1 player has folded. Call update score to update wins, losses and chips.
							pot_of_chips += pot_round;
							pot_round = 0;
							updateScores(vsp);
							throw errorCode::NOT_ENOUGH_PLAYERS;
						}
					
					}
				}
				}
			}
			
			}
		if (numChecked >= (numPlayers - numFolded)){
			a = false;
		}
	}
	//After a round of betting is finished, we want to reset every player's bet to 0 and add the chips from this round to the total chips in the pot.
	for each(shared_ptr<Player> p in vsp){
		(*p).bet = 0;
		
	}
		pot_of_chips += pot_round;
		pot_round = 0;
	return 0;
}

//Changing the number of wins/losses (take vector of shared ptr of players)
void IntermediateGame::updateScores(vector<shared_ptr<Player>> & vsp) {
	vector<shared_ptr<Player>> vspcopy = vsp;

	// find the first playe in the sorted vector that didn't fold
	auto it = vspcopy[vspcopy.size() - 1];
	int i = 1;
	while ((*it).folded) {
		++i;
		it = vspcopy[vspcopy.size() - i];
	}
	// this player won
	++((*(it)).won);
	(*it).chips += pot_of_chips;
	cout << '\n' << (*it) << " Current hand: " << (*it).hand.asString() << endl;

	// every one else lost
	for (auto j = vspcopy.begin(); j != vspcopy.end(); ++j) {
		(**j).bet = 0;
		
		if (j != vspcopy.begin() + (vspcopy.size() - i)) { //for every player that isn't the winner we increment their losses.
			++((**j).lost);
			if (!(**j).folded){
				cout << '\n' << (**j) << " Current hand: " << (**j).hand.asString() << endl; //we print all the losers' hands as long as they didn't fold.
			}
		}
		(**j).folded = false; // Set everyone's fold to false before the next game starts
	}
	pot_of_chips = 0;
	numFolded = 0;
}

//Put all cards back in the main deck
void IntermediateGame::moveCards(vector<shared_ptr<Player>> & vsp) {
	Hand temphand;

	//puts discard deck into main deck
	int tempdiscsize = discard.size();
	for (int i = 0; i < tempdiscsize; ++i){
		temphand << discard;
		deckMain.add_card(temphand[0]);
		temphand.remove_card(0);
	}

	//puts hands into main deck
	for (unsigned int i = 0; i < vsp.size(); ++i){
		auto x = (*vsp[i]).hand.size();
		for (int j = 0; j < x; ++j){
			deckMain.add_card((*vsp[i]).hand[0]);
			(*vsp[i]).hand.remove_card(0);
		}
	}
}

vector<Card> IntermediateGame::seven_choose_five(vector<Card> hand)
{
	// vector of bools tells which five cards to consider
	vector<bool> v = { 0, 0, 0, 0, 0, 1, 1 };
	// store the current highest rank and set of five cards
	Hand h;
	//Hand temp;
	while (next_permutation(v.begin(), v.end())) {
		// choose five of the cards to keep:
		vector<Card> cards; // make a new vector to hold five of the cards
		for (int i = 0; i < 7; ++i) { // iterate through all seven cards
			if (v[i] == 0) { // if this card is being considered
				cards.push_back(hand[i]); // add it to the temp vector of cards
			}
		}
		Hand temp(cards);
		if (poker_Rank(h, temp)){
			h = cards;
		}

	}
	// return the highest ranking set of five cards:
	return h.getVec();
}