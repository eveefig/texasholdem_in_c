Evan Figueroa, Melinda Lai, Mary Richardson
Lab4


The program is run without any arguments other than the program name. The user is them prompted to give the name of a game to play, 
and subsequently asked to add players by name. If an invalid game name is given (other than "FiveCardDraw", "SevenCardStud", or "TexasHoldEm")
an error message is shown and the user is prompted to start another game if they choose.

In our Five card draw game, we rank each hand and print the winning hand first,
but the rest of the hands are printed in unsorted order. We think this implementation
is fine because there is only 1 winner(or multiple if they tie), and the rest are losers,
so there is no significance or benefit to being the first loser or the last loser
In our seven card stud game, there are five turns in each round, and the players are prompted to bet during each turn.
In our Texas hold em game, there are four turns in each round, and the players are prompted to bet during each turn.

Error Messages:
0:  "Success!",
1:  "Error: Incorrect number of arguments",
2:	"Error: The file supplied could not be opened!",
3:	"Error: Vector was empty. No valid cards can be printed!",
4:	"Error: No cards could be loaded into the deck!",
5:	"Error: Hands can not be sorted: Two hands are exactly the same...Someone might be cheating. Below are the unsorted Hands:",
6:	"Error: Either the suit or rank could not be read from the Card!",
7:	"Error: There were not enough cards in the deck to give each player 5 cards. A Deck has 52 cards.",
8:	"Error: The card you are trying to remove does not exist!",
9:	"Error: Could not return instance of Game!",
10:	"Error: There is a game already in progress!",
11:	"Error: The game you tried to start is not recognized. Your game name must contain 'FiveCardDraw' or 'SevenCardStud'!",
12:	"Error: Stop game was called but there is no game currently in progress!",
13:	"Error: There is already a player with this name in the game!",
14:	"Error: Tried inserting cards from an empty deck!",
15:	"Error: You are trying to remove a card outside of the size of your hand!",
16:	"Error: You are trying to play with fewer than 2 players!",
17:	"Error: You have no more cards to remove!",
18:	"Error: Sorry, the response you gave was invalid. Please try again.\n",
19:	"Error: Sorry, you don't have enough chips. Please try again. \n"

Compile Errors and Warnings:
We frogot to add an std scope to shared_ptr<Player> in one of the header files. This gave multiple "missing ;" errors and other misleading errors.
We got a "cannot instantiate abstract class" error because we forgot to omplement one of the methods from the base class in SevenCardStud.
We recieved many mismatching signed/unsigned int comparisons, which were easily resolved by updating the types.
The player chips were being set to a very high value when we attempted to subtract 1 chip for the ante from a player who had no chips.

Test Cases:
We thoroughly tested every possible error that could be thrown, each game that we implemented, and the details to ensure no unexpected results.

Extra Credit:
We implemented Texas Hold Em

