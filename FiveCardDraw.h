#ifndef FIVECARDDRAW_H
#define FIVECARDDRAW_H

#include "stdafx.h"
#include "IntermediateGame.h"
#include "Player.h"

class FiveCardDraw : public IntermediateGame {

public:
	FiveCardDraw();

	int virtual before_turn(Player & pl);
	int virtual turn(Player &);
	int virtual after_turn(Player & pl);

	int call_before_turn();
	int virtual before_round();
	int virtual round();
	int virtual after_round();

	void virtual print(unsigned int yourpos);
};


#endif