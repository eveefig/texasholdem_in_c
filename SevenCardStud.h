#ifndef SEVENCARDSTUD_H
#define SEVENCARDSTUD_H
#include "stdafx.h"
#include "IntermediateGame.h"

#include "Game.h"
#include "Player.h"
#include <string>

class IntermediateGame;
class Game;
class SevenCardStud : public IntermediateGame {

public:
	int virtual first_turn();
	int virtual other_turns();
	void dealeachturn(int numcards);

	int virtual before_round();
	int virtual round();
	int virtual after_round();	

	void print(unsigned int yourpos);


	SevenCardStud();
};


#endif